import numpy as np
from src.AI.Q_Learning.breakout_AI import BreakoutGameAI

possible_state_values = []

boolean_version = True

def get_version():
    return boolean_version

for dis in range(4):
        possible_state_values.append(dis)

def get_state_id(breakout: BreakoutGameAI):
    if boolean_version:
        return _get_state_id_boolean_version(breakout)
    return _get_state_id(breakout)

def _get_state_id(breakout: BreakoutGameAI):
    game = breakout.game
    xDis = game.ball.rect.centerx - game.paddle.rect.centerx
    state = [
        xDis
    ]

    state = np.array(state, dtype=int) #dtype=int converts true or false to ints
    return state[0]

def _get_state_id_boolean_version(breakout: BreakoutGameAI):
    game = breakout.game
    xDis = game.ball.rect.centerx - game.paddle.rect.centerx

    ball_left = xDis < 0

    state = [
        ball_left
    ]

    state = np.array(state, dtype=int) #dtype=int converts true or false to ints
    state_id = _get_state_id_from_state_boolean_version(state)
    return state_id

def _get_state_id_from_state_boolean_version(state):
    first = "".join(map(str, state))
    first = int(first, 2)

    state_value = (first)
    state_id = possible_state_values.index(state_value)
    return state_id