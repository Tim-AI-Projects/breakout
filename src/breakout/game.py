from ..breakout.constants.game_constants import LIVES
from ..breakout.constants.sprite_setup import SpriteSetup
import pygame

class Game():
    def __init__(self, pygame: pygame):
        self.lives = LIVES
        self.score = 0
        self.level = 1
        self.bg = None
        self.setup = SpriteSetup(pygame)
        self.pygame = self.setup.pygame
        self.screen = self.setup.screen
        self.all_sprites_list = self.setup.all_sprites_list
        self.all_bricks = self.setup.all_bricks
        self.ball = self.setup.ball
        self.paddle = self.setup.paddle
        pygame.mixer.music.load("src/breakout/sounds/Exponential_Entropy.mp3")
        pygame.mixer.music.set_volume(0.3) 
        pygame.mixer.music.play(-1,0.0)
        self.reward = 0
        self.time_since_last_brick_hit = 0
        self.total_game_time = 0
        
        # The clock will be used to control how fast the screen updates
        self.clock = self.pygame.time.Clock()
        self.last_game_ended = 0
        self.time_elapsed = 0

    def lives(self):
        return self.lives

    def score(self):
        return self.score

    def level(self):
        return self.level

    def bg(self):
        return self.bg

    def bg(self, value):
        self.bg = value

    def level(self, value):
        self.level = value

    def score(self, value):
        self.score = value

    def lives(self, value):
        self.lives = value