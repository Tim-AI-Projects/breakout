from ...breakout.constants.game_constants import MOVE_TO_BALL_REWARD
from ...breakout.game import Game
from ...breakout.screen_manager import ScreenManager
from ...breakout.sprites.paddle import Paddle
from ...breakout.constants.screen_constants import *
import numpy as np
import pygame

class PlayerInput():
    def process_player_input(pygame: pygame, paddle: Paddle, keys, screen: ScreenManager):
        if keys[pygame.K_DOWN]:
            screen.paused_game = True #Pause the game

        if keys[pygame.K_LEFT] and not screen.paused_game:
            paddle.moveLeft(PADDLE_SPEED) #Move left
        
        if keys[pygame.K_RIGHT] and not screen.paused_game:
            paddle.moveRight(PADDLE_SPEED) #Move right

    def process_AI_input(game: Game, action, screen: ScreenManager): #[left, right, nothing]
        paddle = game.paddle
        xDis = game.ball.rect.centerx - paddle.rect.centerx
        ball_left = xDis < 0


        screen.paused_game = False
        if np.array_equal(action, [1, 0, 0]):
            paddle.moveLeft(PADDLE_SPEED)
            if ball_left:
                game.reward += MOVE_TO_BALL_REWARD
            else:
                game.reward -= MOVE_TO_BALL_REWARD

        if np.array_equal(action, [0, 1, 0]):
            paddle.moveRight(PADDLE_SPEED)
            if ball_left:
                game.reward -= MOVE_TO_BALL_REWARD
            else:
                game.reward += MOVE_TO_BALL_REWARD

        if np.array_equal(action, [0, 0, 1]):
            pass