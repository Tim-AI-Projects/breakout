from ..sprites.brick import Brick
from ..constants.game_constants import *
from ..constants.colors import *
from ..constants.screen_constants import *

class BrickSetup():
    def __init__(self):
        pass

    def next_level(self, level, all_sprites_list, all_bricks):
        if level == 2:
            self.setup_bricks_level_2(all_sprites_list, all_bricks)
        elif level == 3:
            self.setup_bricks_level_3(all_sprites_list, all_bricks)
        elif level == 4:
            self.setup_bricks_level_4(all_sprites_list, all_bricks)
        else:
            self.setup_bricks_level_4(all_sprites_list, all_bricks)

    def setup_bricks_level_1(self, all_sprites_list, all_bricks):
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2) #80, 30
            brick.rect.x = BRICKS_START_X + i * BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y # base = 60
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
            brick.rect.x = BRICKS_START_X + i * BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 1) # Base = 100
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
            brick.rect.x = BRICKS_START_X + i* BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 2)
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
            brick.rect.x = BRICKS_START_X + i* BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 3)
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        # for i in range(BRICK_COUNT):
        #     brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
        #     brick.rect.x = 60 + i* 100
        #     brick.rect.y = 220
        #     all_sprites_list.add(brick)
        #     all_bricks.add(brick)
        return all_bricks

    def setup_bricks_level_2(self, all_sprites_list, all_bricks):
        for i in range(BRICK_COUNT):
            brick = Brick(RED, BRICK_WIDTH, BRICK_HEIGHT, 2) 
            brick.rect.x = BRICKS_START_X + i* BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 1)
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
            brick.rect.x = BRICKS_START_X + i* BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 4)
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        return all_bricks

    def setup_bricks_level_3(self, all_sprites_list, all_bricks):
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
            brick.rect.x = BRICKS_START_X + i* BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 4)
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
            brick.rect.x = BRICKS_START_X + i* BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 6)
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        return all_bricks

    def setup_bricks_level_4(self, all_sprites_list, all_bricks):
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
            brick.rect.x = BRICKS_START_X + i* BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 1)
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
            brick.rect.x = BRICKS_START_X + i* BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 5)
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
            brick.rect.x = BRICKS_START_X + i* BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 6)
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        return all_bricks

    def setup_bricks_level_5(self, all_sprites_list, all_bricks):
        for i in range(BRICK_COUNT):
            brick = Brick(YELLOW, BRICK_WIDTH, BRICK_HEIGHT, 2)
            brick.rect.x = BRICKS_START_X + i* BRICK_X_DIFF
            brick.rect.y = BRICKS_START_Y + (BRICK_Y_DIFF * 1)
            all_sprites_list.add(brick)
            all_bricks.add(brick)
        return all_bricks

