from ...breakout.game import Game
from ...breakout.logic.collision_detection import CollisionDetection
from ...breakout.constants.game_constants import *
from ...breakout.screen_manager import ScreenManager
from ...breakout.logic.brick_setup import BrickSetup
from ...breakout.constants.screen_constants import *
import random
import pygame

class GameLogic():
    def __init__(self):
        self.collisionDetection = CollisionDetection()
        self.screenManager = ScreenManager()
        self.brickSetup = BrickSetup()

    def handle_ball_movement(self, game: Game, pygame: pygame):
        game.all_sprites_list.update()

        game.lives = self.collisionDetection.check_wall_collision(game.ball, game.lives, game.paddle, game)
        if game.lives == 0:
            self.screenManager.game_over_screen(game.screen, pygame, game)
            #Reset the game
            return False

        self.collisionDetection.check_paddle_collision(pygame, game.ball, game.paddle, game)

        #Check if there is the ball collides with any of bricks
        brick_collision_list = pygame.sprite.spritecollide(game.ball, game.all_bricks, False)
        if self.collisionDetection.check_bricks_collision(brick_collision_list, game.ball, game, game.all_bricks, pygame, game.screen):
            if game.level == LEVELS + 5:
                game.reward += VICTORY_REWARD
                self.screenManager.victory_screen(game.screen, pygame, game)
                return False
            else:
                self.__next_level(game)
        return True

    def reset_game(self, game: Game):
        self.__reset_paddle(game)
        self.__reset_ball(game)
        game.all_bricks = game.pygame.sprite.Group()
        game.all_sprites_list = game.pygame.sprite.Group()
        game.all_sprites_list.add(game.paddle)
        game.all_sprites_list.add(game.ball)
        game.lives = LIVES
        game.score = 0
        game.level = 1
        game.reward = 0
        game.time_since_last_brick_hit = 0
        game.total_game_time = 0
        game.last_game_ended = game.pygame.time.get_ticks()
        self.brickSetup.setup_bricks_level_1(game.all_sprites_list, game.all_bricks)

    def __next_level(self, game: Game):
        game.level += 1
        self.__reset_paddle(game)
        self.__reset_ball(game)
        game.all_bricks = game.pygame.sprite.Group()
        self.brickSetup.next_level(game.level, game.all_sprites_list, game.all_bricks)

    def __reset_paddle(self, game: Game):
        game.paddle.rect.x = PADDLE_XPOS #Start Xpos
        game.paddle.rect.y = PADDLE_YPOS #Start Ypos
        
    def __reset_ball(self, game: Game):
        game.ball.rect.x = BALL_XPOS
        game.ball.rect.y = BALL_YPOS
        game.ball.velocity[0] = 2 * BALL_SPEED if random.random() < 0.5 else -2 * BALL_SPEED
        game.ball.velocity[1] = 4 * BALL_SPEED