# Open a new window (width, height)
from ...breakout.sprites.brick import Brick
from ...breakout.sprites.ball import Ball
from ...breakout.logic.brick_setup import BrickSetup
from ...breakout.constants.colors import *
from ...breakout.sprites.paddle import Paddle
from ...breakout.constants.game_constants import *
from ...breakout.constants.screen_constants import *

class SpriteSetup():
    def __init__(self, pygame):
        self.brickSetup = BrickSetup()
        self.pygame = pygame

        size = (SCREEN_WIDTH, SCREEN_HEIGHT)
        self.screen = pygame.display.set_mode(size)
        self.pygame.display.set_caption("Breakout game")

        #This will be a list that will contain all the sprites we intend to use in our game.
        self.all_sprites_list = pygame.sprite.Group()

        #Create the Paddle
        self.paddle = Paddle(WHITE, PADDLE_WIDTH, PADDLE_HEIGHT)
        self.paddle.rect.x = PADDLE_XPOS #Start Xpos
        self.paddle.rect.y = PADDLE_YPOS #Start Ypos

        self.ball = Ball(WHITE, BALL_WIDTH, BALL_HEIGHT)
        self.ball.rect.x = BALL_XPOS
        self.ball.rect.y = BALL_YPOS

        self.all_bricks = pygame.sprite.Group()
        self.brickSetup.setup_bricks_level_1(self.all_sprites_list, self.all_bricks)

        # Add the paddle to the list of sprites
        self.all_sprites_list.add(self.paddle)
        self.all_sprites_list.add(self.ball)