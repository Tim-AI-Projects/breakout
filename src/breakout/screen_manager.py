from ..breakout.constants.colors import *
from ..breakout.constants.game_constants import *
from ..breakout.constants.screen_constants import *
from math import floor

class ScreenManager():
    def __init__(self):
        self.paused_game = False
        self.skip_loading = True

    def level_complete(self, screen, pygame, game):
        self.update_screen(game)
        self.show_text_screen(screen, pygame, 'LEVEL COMPLETE', 'src/breakout/sounds/fullmetal_alchemist.mp3', 250, 300, 74, 3000)

    def game_over_screen(self, screen, pygame, game):
        self.update_screen(game)
        self.show_text_screen(screen, pygame, 'GAME OVER', 'src/breakout/sounds/game-over.wav', 250, 300, 74, 3000)

    def victory_screen(self, screen, pygame, game):
        self.update_screen(game)
        self.show_text_screen(screen, pygame, 'YOU WON', 'src/breakout/sounds/fullmetal_alchemist.mp3', 250, 300, 74, 3000)

    def update_screen(self, game):
        # --- Screen design
        #SCREEN_WIDTH = 900 #Base = 900
        #SCREEN_HEIGHT = 800 #Base = 800
        game.screen.fill(BLACK)
        game.screen.blit(game.pygame.transform.scale(game.bg, (SCREEN_WIDTH, SCREEN_HEIGHT)), (0,(SCREEN_HEIGHT / 21.1)))
        game.pygame.draw.line(game.screen, WHITE, [0, (SCREEN_HEIGHT / 21.1)], [SCREEN_WIDTH, (SCREEN_HEIGHT / 21.1)], 2)
        

        #Display the score and the number of lives at the top of the screen
        font = game.pygame.font.Font(None, floor(SCREEN_HEIGHT / 23.6))
        text = font.render("Score: " + str(game.score), 1, WHITE)
        game.screen.blit(text, ((SCREEN_WIDTH / 45), (SCREEN_HEIGHT / 80)))
        text = font.render("Lives: " + str(game.lives), 1, WHITE)
        game.screen.blit(text, ((SCREEN_WIDTH / 1.125), (SCREEN_HEIGHT / 80)))

        if self.paused_game:
            font = game.pygame.font.Font(None, 60)
            text = font.render('The game is paused', 74, WHITE)
            game.screen.blit(text, ((SCREEN_WIDTH/8), SCREEN_HEIGHT /2))

         

    def show_text_screen(self, screen, pygame, text, sound, width, height, size, wait):
        if self.skip_loading:
            return

        font = pygame.font.Font(None, size)
        text = font.render(text, 1, WHITE)
        screen.blit(text, (width,height))

        if sound != None:
            effect = pygame.mixer.Sound(sound)
            effect.set_volume(0.2)
            effect.play()

        pygame.display.flip()
        pygame.time.wait(wait)