from ...breakout.constants.game_constants import BALL_SPEED
from ..constants.colors import *
import pygame
 
class Ball(pygame.sprite.Sprite):
    #This class represents a ball. It derives from the "Sprite" class in Pygame.
    
    def __init__(self, color, width, height):
        # Call the parent class (Sprite) constructor
        super().__init__()
        
        # Pass in the color of the ball, its width and height.
        # Set the background color and set it to be transparent
        self.image = pygame.Surface([width, height])
        self.image.fill(BLACK)
        self.image.set_colorkey(BLACK)
 
        # Draw the ball (a rectangle!)
        pygame.draw.rect(self.image, color, [0, 0, width, height])
        velocityX = 2 * BALL_SPEED
        velocityY = 4 * BALL_SPEED
        self.velocity = [velocityX, velocityY]#[randint(4,8),randint(-8,8)] # x as speed (- is left and + is right) and y as speed (- is down and + is up)
        
        # Fetch the rectangle object that has the dimensions of the image.
        self.rect = self.image.get_rect()
        
    def update(self):
        self.rect.x += self.velocity[0]
        self.rect.y += self.velocity[1]

    def bounce(self, velocityX, velocityY = (-4 * BALL_SPEED)):
        self.velocity[0] = velocityX #x velocity
        self.velocity[1] = velocityY #randint(-8,8) y velocity