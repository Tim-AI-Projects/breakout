
<div id="top"></div>


<!-- PROJECT SHIELDS 
[![Forks][forks-shield]][forks-url]
[![Issues][issues-shield]][issues-url]
-->


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/Tim-AI-Projects/breakout/-/blob/main/README.md">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Atari Breakout AI</h3>

  <p align="center">
    Using reinforcement learning to make an AI play Atari Breakout!
    <br />
    <a href="https://gitlab.com/Tim-AI-Projects/breakout/-/blob/main/README.md"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <!-- <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a> -->
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
<div align="center">
    <img src="images/Atari_breakout.jpg" alt="Logo">
</div>

### Breakout AI
Breakout begins with eight rows of bricks, with each two rows a different kind of colour. Using a single ball, the player must knock down as many bricks as possible by using the walls and/or the paddle below to hit the ball against the bricks and eliminate them. If the player's paddle misses the ball's rebound, they will lose.  
The game is created with Python and Pygame. The different AIs are created in Python with Q-Learning, Deep Q-Learning. These different methods/Algorithms are going to be compared with each other to see the different up and down sides. 

- A more detailed documentation about this project can be found here: [Q-Learning process](https://stichtingfontys-my.sharepoint.com/:w:/g/personal/409997_student_fontys_nl/ETLPJaQcxZlMkx8cXJqh9M8B0-IS4tO1_b_BMUTw0nwGKw?e=T7uWxV), here [Deep Q-Learning process](https://stichtingfontys-my.sharepoint.com/:w:/g/personal/409997_student_fontys_nl/EQi12pM029JKpNzlSBbYCCwBA0jjPe9TJcuK010ZyUw33w?e=0YRQ9O) and here [Machine Learning Breakout Comparison](https://stichtingfontys-my.sharepoint.com/:w:/g/personal/409997_student_fontys_nl/EZgjt7S6AM5Jse1Eg8tksc0BDzzVctKG-caQwhMnvNQZJg?e=aGA4rG) 
- The personal challenge project plan can be found here: [Project plan](https://stichtingfontys-my.sharepoint.com/:w:/g/personal/409997_student_fontys_nl/EUkskXcqbR9Mn2nJwiRHCFAB7wu-7nJsoLH3onf03pCoFQ?e=dWOBTe)
- The other repositories can be found here: [Tim AI Projects](https://gitlab.com/Tim-AI-Projects)

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

This section lists any major frameworks/libraries used for the project.

* [Pygame](https://www.pygame.org/)
* [Python](https://www.python.org/)
* [Tensorflow](https://www.tensorflow.org/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* Install [Python](https://realpython.com/installing-python/) 

### Installation

_Below is an example of how you can install and setup the app._

1. Clone the repo
   ```sh
   git clone https://gitlab.com/Tim-AI-Projects/breakout.git
   ```
2. Install pip packages
   ```sh
   pip install -r requirements.txt
   ```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

This shows an example of how to use the app.

To play the game use: 

```sh
python breakout_game.py
```
To run the AI use: 
```sh
python Deep_Q_Learning_agent.py or python Q_Learning_agent.py
```


<p align="right">(<a href="#top">back to top</a>)</p>


<!-- CONTRIBUTING -->
## Contributing

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Project Link: [https://gitlab.com/Tim-AI-Projects](https://gitlab.com/Tim-AI-Projects)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://gitlab.com/Tim-AI-Projects/openaigymprojects/-/forks/new
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://gitlab.com/Tim-AI-Projects/openaigymprojects/-/blob/main/LICENSE.txt
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://gitlab.com/Tim-AI-Projects/openaigymprojects/-/issues
