from AI.Q_Learning.breakout_AI import BreakoutGameAI
from collections import deque
from collections import deque
from AI.Q_Learning.model import Linear_QNet, QTrainer
from AI.helper import plot
import numpy as np
import random
import torch
import random

MAX_MEMORY = 800_000
BATCH_SIZE = 1000
LEARNING_RATE = 0.001

class DeepQLearningAgent:
    def __init__(self):
        self.number_of_games = 0
        self.epsilon = 0.20 # controls randomness was 0.7
        self.gamma = 0.9 # discount rate must be smaller than 1
        self.memory = deque(maxlen=MAX_MEMORY) # popleft()
        self.model = Linear_QNet(6, 256, 3) # first is the size of the states (input is 6 for now see get_state) and the second is the hidden while the last is the output (3 actions)
        self.trainer = QTrainer(self.model, lr=LEARNING_RATE, gamma=self.gamma)

    def get_state(self, breakout: BreakoutGameAI):
        game = breakout.game

        ball_x_velocity = game.ball.velocity[0]
        ball_y_velocity = game.ball.velocity[1]

        ball_x = game.ball.rect.centerx
        ball_y = game.ball.rect.centery
        paddle = game.paddle.rect.centerx

        dis = abs(game.ball.rect.centerx - game.paddle.rect.centerx)

        state = [
            ball_x, ball_y, paddle, dis, ball_y_velocity, ball_x_velocity
        ]

        return np.array(state, dtype=int) #dtype=int converts true or false to ints

    def remember(self, state, action, reward, next_state, game_over):
        self.memory.append((state, action, reward, next_state, game_over)) # popleft if MAX_MEMORY is reached

    def train_long_memory(self):
        if len(self.memory) > BATCH_SIZE:
            mini_sample = random.sample(self.memory, BATCH_SIZE) # list of tuples 
        else:
            mini_sample = self.memory
        
        states, actions, rewards, next_states, game_overs = zip(*mini_sample)
        self.trainer.train_step(states, actions, rewards, next_states, game_overs)

    def train_short_memory(self, state, action, reward, next_state, game_over):
        self.trainer.train_step(state, action, reward, next_state, game_over)

    def get_action(self, state, total_game_time):

        if self.number_of_games > 30: #and total_game_time < 200*10: # Around 12 seconds == 200*10
            self.epsilon = 0.05
        else:
            self.epsilon = 0.2

        final_move = [0, 0, 0]
        if random.uniform(0, 1) < self.epsilon and self.number_of_games % 50 != 0: # smaller epsilon the less frequent it will happen (decides a random move or a 'good' move)
            move = random.randint(0, 2)
            final_move[move] = 1
        else:
            state0 = torch.tensor(state, dtype=torch.float)
            predition = self.model(state0)
            move = torch.argmax(predition).item()
            final_move[move] = 1

        return final_move

def train():
    plot_scores = []
    plot_mean_scores = []
    total_score = 0
    record_score = 0
    record_reward = 0
    agent = DeepQLearningAgent()
    breakout = BreakoutGameAI()
    while True:
        state_old = agent.get_state(breakout)
        action = agent.get_action(state_old, breakout.game.total_game_time)

        reward, game_over, score = breakout.play_step(action, agent.number_of_games)
        state_new = agent.get_state(breakout)

        agent.train_short_memory(state_old, action, reward, state_new, game_over)
        agent.remember(state_old, action, reward, state_new, game_over)

        if game_over:
            # train long memory, plot result
            agent.number_of_games += 1
            agent.train_long_memory()

            if reward > record_reward:
                record_reward = reward

            if score > record_score:
                record_score = score
                agent.model.save()
            agent.number_of_games, 
            if agent.number_of_games % 10 == 0:
                print('Game', agent.number_of_games, 'Reward', reward, 'Record reward:', record_reward, "Score:", score, 'Record score:', record_score, "Epsilon:", agent.epsilon)

            #if agent.number_of_games % 10 == 0:
            plot_scores.append(score)
            total_score += score
            mean_score = total_score / agent.number_of_games
            plot_mean_scores.append(mean_score)
            plot(plot_scores, plot_mean_scores)
            breakout.gameLogic.reset_game(breakout.game)
        
if __name__ == '__main__':
    train()